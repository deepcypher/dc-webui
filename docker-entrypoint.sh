#!/usr/bin/env bash
# @Author: George Onoufriou <archer>
# @Date:   2021-11-05T14:34:33+00:00
# @Last modified by:   archer
# @Last modified time: 2021-11-22T13:15:12+00:00

gunicorn --workers=2 --chdir /app --bind 0.0.0.0:80 --log-level info --access-logformat '%(h)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s" "%(D)s' --keep-alive 10 "app:entry_point()"
