//usr/bin/go run $0 $@; exit $?
/**
 * @Author: George Onoufriou <archer>
 * @Date:   2021-11-22T13:54:14+00:00
 * @Last modified by:   archer
 * @Last modified time: 2021-11-25T15:13:54+00:00
 */
package main

import (
	"html/template"
	"path/filepath"

	"github.com/Masterminds/sprig"
	"github.com/gin-gonic/gin"
	"gitlab.com/deepcypher/dc-webui/goui/endpoints"
)

func main() {
	router := gin.Default()
	// Loading templates without custom template renderer
	// router.LoadHTMLGlob("templates/**.html")
	// Loading templates with custom SPRIG template renderer
	// https://stackoverflow.com/questions/11467731/is-it-possible-to-have-nested-templates-in-go-using-the-standard-library
	html := template.Must(
		template.New("tmpls").Funcs(sprig.FuncMap()).ParseGlob("templates/**/*.html"))
	router.SetHTMLTemplate(html)
	router.Static("/static", "./static")
	router.StaticFile("/favicon.ico", "./static/img/favicon.ico")
	router.StaticFile("/robots.txt", "./static/robots/robots.txt")
	router.StaticFile("/humans.txt", "./static/humans/humans.txt")

	// If any exist add static route for google verification of ownership
	addStaticGlob("./static/robots/google*.html", router)

	endpoints.SetEndpoints(router)
	router.Run(":8080") // listen and serve on 0.0.0.0:8080 by default
}

func addStaticGlob(glob string, router *gin.Engine) {
	files, err := filepath.Glob(glob)
	if err != nil {
		panic(err.Error)
	}
	for _, file := range files {
		router.StaticFile("/"+filepath.Base(file), file)
	}

}
