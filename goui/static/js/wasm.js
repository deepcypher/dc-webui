const goWasm = new Go()

WebAssembly.instantiateStreaming(fetch("/static/wasm/main.wasm"), goWasm.importObject)
  .then((result) => {
    goWasm.run(result.instance)
  })
