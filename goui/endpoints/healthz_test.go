/**
 * @Author: George Onoufriou <archer>
 * @Date:   2021-11-23T15:06:26+00:00
 * @Last modified by:   archer
 * @Last modified time: 2021-11-23T18:38:57+00:00
 */
package endpoints

import (
    "fmt"
    "testing"
    "io/ioutil"
    "net/http"
    "net/http/httptest"
    "github.com/gin-gonic/gin"
)

// TestLanding ensure Landing func returns
// valid html and 200
func TestHealthz(t *testing.T) {
    gin.SetMode(gin.ReleaseMode)
    //  create a new recorder to test our funcs with
    recorder := httptest.NewRecorder()
    context, _ := gin.CreateTestContext(recorder)
    healthz(context)
    response := recorder.Result()
    //  Check HTTP response code success
    if recorder.Code != http.StatusOK {
      t.Errorf("expected HTTP:200 code got %v", recorder.Code)
    }
    // TODO: Read in body and ensure is as expected
    defer response.Body.Close()
    data, err := ioutil.ReadAll(response.Body)
    if err != nil {
      t.Errorf("expected error to be nil got %v", err)
    }
    // if string(data) != "{'message':'OK'}" {
    //     t.Errorf("expected ABC got %v", string(data))
    // }
    fmt.Println(string(data))
}
