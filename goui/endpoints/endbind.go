/**
 * @Author: George Onoufriou <archer>
 * @Date:   2021-11-22T21:41:18+00:00
 * @Last modified by:   archer
 * @Last modified time: 2021-11-23T18:37:23+00:00
 */
package endpoints

import (
  "github.com/gin-gonic/gin"
)

func SetEndpoints(r *gin.Engine) {
  r.GET("/", landing)
  r.GET("/about", about)
  r.GET("/docs", docs)
  r.GET("/login", login)
  // r.GET("/settings", conf)
  // Inform kube we are healthy but not necessarily ready
  r.GET("/healthz", healthz)
  // Inform kube or other watcher that app is ready
  // this differs from healthz in that this means it
  // can reach things like the API which it may not
  // need to in tests etc
  r.GET("/readyz", readyz)
}
