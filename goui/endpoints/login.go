/**
 * @Author: George Onoufriou <archer>
 * @Date:   2021-11-23T14:59:37+00:00
 * @Last modified by:   archer
 * @Last modified time: 2021-11-24T10:37:40+00:00
 */
package endpoints

import (
    "net/http"
    "github.com/gin-gonic/gin"
)

func login(c *gin.Context) {
    c.HTML(http.StatusOK, "login.tmpl.html", gin.H{
      "title": "Login",
    })
}
