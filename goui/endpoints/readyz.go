/**
 * @Author: George Onoufriou <archer>
 * @Date:   2021-11-23T14:59:37+00:00
 * @Last modified by:   archer
 * @Last modified time: 2021-11-23T16:53:07+00:00
 */
package endpoints

import (
    "net/http"
    "github.com/gin-gonic/gin"
)

func readyz(c *gin.Context) {
    c.JSON(http.StatusOK, gin.H{"message": "OK"})
}
