# @Author: George Onoufriou <archer>
# @Date:   2021-11-05T15:48:15+00:00
# @Last modified by:   archer
# @Last modified time: 2021-11-22T13:12:45+00:00

import os
import flask
import flask_assets
import logging
import random
import string
import configargparse as argparse


def get_argparser(description):
    """Generate parser for confirg and env args for app."""
    description = description if description is not None else \
        "deepcypher-webui default arg-parser"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument("--hostname",
                        type=str,
                        default="deepcypher.me",
                        env_var="DC_UI_HOSTNAME",
                        help="Set the name this server will listen for.")
    parser.add_argument("-s", "--secret",
                        type=str,
                        default=gen_random(),
                        env_var="DC_UI_SECRET",
                        help="Set the UI secret key for cookies etc.")
    parser.add_argument("-i", "--api",
                        type=str,
                        default="api.deepcypher.me",
                        env_var="DC_UI_API",
                        help="IP or hostname where API is reachable.")
    parser.add_argument("-p", "--api-port",
                        type=int,
                        default=443,
                        env_var="DC_UI_API_PORT",
                        help="Port where API is reachable.")
    parser.add_argument("-l", "--log-level",
                        type=int,
                        default=logging.INFO,  # logging.INFO == 20
                        env_var="DC_UI_LOG_LEVEL",
                        help="Set log level which to print messages with.")
    return parser


def gen_random(len=60):
    """Generate random alphanumeric sequence for secret key."""
    x = ''.join(
        random.choice(
            string.ascii_uppercase + string.ascii_lowercase + string.digits)
        for _ in range(len))
    logging.debug("secret_key: {}".format(x))
    return x


def arg_handler(argv, description: str = None):
    """Quick argument handler just to make everything easier to work with."""
    parser = get_argparser(description)

    args = vars(parser.parse_args(argv))
    # spinning up logging
    logging.basicConfig(  # filename="{}.log".format(__file__),
        level=args["log_level"],
        format="%(asctime)s %(levelname)s:%(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S")
    logging.debug("initial args: {}".format(args))
    return args
