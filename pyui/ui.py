# @Author: George Onoufriou <archer>
# @Date:   2021-11-22T13:33:00+00:00
# @Last modified by:   archer
# @Last modified time: 2021-11-22T13:39:56+00:00

import flask
import logging


def ui_blueprint_factory():
    """Create a webui blueprint for flask attatchment."""
    web_bp = flask.Blueprint(
        "pyui", __name__,
        template_folder="templates",
        static_folder="static")

    @web_bp.route("/")
    def index():
        logging.debug("SESSION={}".format(flask.session))
        return flask.render_template("landing.jinja")

    # return now fully instantiated blueprint
    return web_bp
