# @Author: George Onoufriou <archer>
# @Date:   2021-11-05T15:44:35+00:00
# @Last modified by:   archer
# @Last modified time: 2021-11-22T13:45:39+00:00

import os
import sys
import flask
import flask_assets
import logging
import random
import string
from utils import arg_handler
from ui import ui_blueprint_factory


def flask_factory(secret_key, server_name):
    """Create basic flask object."""
    app = flask.Flask(__name__, template_folder=None, static_folder=None)
    app.secret_key = secret_key
    app.config["SERVER_NAME"] = server_name
    return app


def entry_point(argv=None):
    """Entrypoint for external WSGI servers like gunicorn."""
    argv = argv if argv is not None else sys.argv[1:]
    print(argv)
    args = arg_handler([])  # ignoring argv as wsgi server could have same vars
    app = flask_factory(secret_key=args["secret"],
                        server_name=args["hostname"])
    ui = ui_blueprint_factory()
    app.register_blueprint(ui)
    logging.info("Launching flask webserver...")
    logging.info("Routes: \n{}".format(app.url_map))
    return app
