#!/usr/bin/env bash

# @Author: George Onoufriou <archer>
# @Date:   2021-11-25T09:01:30+00:00
# @Last modified by:   archer
# @Last modified time: 2021-11-25T09:02:12+00:00

sudo docker build --build-arg GIN_MODE="release" -t a/dc-web -f Gockerfile . && \
sudo docker run -p 127.0.0.1:8080:8080 -it a/dc-web
