SUBMAKE_DIR="goui/"

.PHONY: all
all: test

.PHONY: build
build:
	$(MAKE) -C ${SUBMAKE_DIR} build

.PHONY: run
run:
	$(MAKE) -C ${SUBMAKE_DIR} run

.PHONY: test
test:
	$(MAKE) -C ${SUBMAKE_DIR} test

.PHONY: clean
clean:
	$(MAKE) -C ${SUBMAKE_DIR} clean

.PHONY: dbuild
dbuild:
	sudo docker build --build-arg GIN_MODE="release" -t a/dc-webui -f Gockerfile .

.PHONY: drun
drun: dbuild
	sudo docker run -p 127.0.0.1:8080:8080 -it a/dc-web

.PHONY: dtest
dtest:
